#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int first[4000] = {0}, second[4000] = {0}, third[4000] = {0};
	int f_count, s_count, t_count;
	int i, j, k;
	int counter = 0;
	
	scanf("%d", &f_count);
	for (i = 0; i < f_count; i++)
		scanf("%d", &first[i]);
		
	scanf("%d", &s_count);
	for (j = 0; j < s_count; j++)
		scanf("%d", &second[j]);
	
	scanf("%d", &t_count);
	for (k = 0; k < t_count; k++)
		scanf("%d", &third[k]);
	
	i = 0; j = 0; k = 0;
	while(1)
	{
		if(first[i] == second[j] && second[j] == third[k])
		{
			counter++;
			if (i < f_count - 1 && j < s_count - 1 && k < t_count - 1)
			{	i++; j++; k++;	}
			else
				break;
		}
		else
		{
			if (first[i] < second[j] || first[i] < third[k])
			{	if (i < f_count - 1)		i++;
				else 						break;	}
			if (second[j] < first[i] || second[j] < third[k])
			{	if (j < s_count - 1)		j++;
				else 						break;	}
			if (third[k] < first[i] || third[k] < second[j])
			{	if (k < t_count - 1)		k++;
				else 						break;	}
		}
	}
	printf("%d", counter);
	return 0;
}
