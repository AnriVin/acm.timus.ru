#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int num; 
	int i = 0, j = 0, answer[10] = {0};
	scanf("%d", &num);
		
	if (num == 0)
	{
		printf("10\n");
		return 0;
	}
	if (num < 10)
	{
		printf("%d\n", num);
		return 0;
	}
	for  (i = 9; i > 1; i--)
	{
		while (num % i == 0)
		{
			num /= i;
			answer[i]++;
		}
	}
	if (num != 1) { printf("-1"); }
	else
	for (i = 2; i < 10; i++) {
		for (j = 0; j < answer[i]; j++) {
			putc('0' + i, stdout);
		}
	}
	
	putc('\n', stdout);
	return 0;
}
