#include <stdio.h>
#include <stdlib.h>

struct city
{
    int x;
    int y;
    int num;
};

int main(int argc, char **argv)
{
    int N, i, j;
    struct city cities[10000], temp;

    scanf("%d", &N);
    for (i = 0; i < N; i++)
    {
        scanf("%d%d", &cities[i].x, &cities[i].y);
        cities[i].num = i + 1;
    }
    for (i = 0; i < N - 1; i++)
        for (j = i + 1; j < N; j++)
            if (cities[i].x > cities[j].x)
            {
                temp = cities[i];
                cities[i] = cities[j];
                cities[j] = temp;
            }
    for (i = 0; i < N; i++)
    {   
        printf("%d %d\n", cities[i].num, cities[i + 1].num);
        i++;
    }
    return 0;
}
